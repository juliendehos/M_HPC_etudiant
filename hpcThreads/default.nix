
let 

  _pkgs = import <nixpkgs> { 
    config = {
      packageOverrides = pkgs: {

        pybind11 = pkgs.callPackage ../pybind11.nix { 
          inherit pkgs;
          python = pkgs.python3;
        };

      };
    };
  };

  _pyPkgs = _pkgs.python3Packages;

in 

_pyPkgs.buildPythonPackage {
  name = "hpcThreads";
  src = ./.;
  buildInputs = [ 
    _pkgs.pybind11 
    _pyPkgs.matplotlib
    _pyPkgs.numpy
  ];
}
